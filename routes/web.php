<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('main_welcome');  })->name('main_welcome');

//Route::get('/', function () { return view('main_welcome');  })->name('main_welcome');

//Auth::routes();

//Route::get('/', 'MainPageController@index')->name('main_welcome');



Route::get('/test', function(){

//    echo"TEST";
    return view('test');
});

Route::get('/testone/{num}', function($num){


    echo"TEST ONE ".$num ;
});


Route::match(['get', 'post'],'/sendemail', 'HomeController@email')->name('send_email');


//article
Route::group(['prefix' => 'article'], function () {
    //users List
//    Route::get('/', ['uses' => 'Admin\AdminUserController@execute', 'as' => 'users']);
    //article/show/2
    Route::match(['get', 'post'], '/show/{id}', ['uses' => 'ArticleController@create', 'as' => 'article_show']);
    //article/add
    Route::match(['get', 'post'], '/create', ['uses' => 'ArticleController@create', 'as' => 'article_create']);
    //article/edit/2
//    Route::match(['get', 'post', 'delete'], '/edit/{id}', ['uses' => 'ArticleController@create', 'as' => 'article_edit']);
});


//admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/user', ['uses' => 'Admin\AdminController@index', 'as' => 'user']);

    Route::match(['get','post'], '/user/create', ['uses' => 'Admin\AdminController@create', 'as' => 'user_create']);

    Route::get('/articles', ['uses' => 'ArticleController@index', 'as' => 'articles_list']);

    Route::match(['get', 'post','delete'], '/article/edit/{id}', ['uses' => 'ArticleController@edit', 'as' => 'article_edit']);
});


/////////////////////////////////////////////////////


Route::redirect('/', '/en');

Route::group(['prefix' => '{language}', 'middleware' => 'lang'], function(){

    Auth::routes();

    Route::get('/', 'MainPageController@index')->name('main_welcome');

});



//Route::get('/lang', function(){
//    App::setLocale('en');
//    dd(App::getLocale());
//});































