@extends('layouts.main_app')

@section('users')

    <div class="text-center"><a href="{{ route('user_create') }}">Create User</a></div>

    <div class="text-center"><h4>Users List</h4></div>
    <div class="container-fluid">
        @if ($users)
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>role</th>
                    <th>email</th>
                    <th>Data Created</th>
                    <th>Delete</th>
                </tr>
                </thead>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id}}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>Delete
                        {{--{!!  Form::open(['url' => route('userEdit', ['user'=>$user->id]), 'class'=>'form-horizontal', 'method'=>'POST']) !!}--}}
                        {{--{{ method_field('DELETE') }}--}}
                        {{--{!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}--}}
                        {{--{!! Form::close() !!}--}}
                    </td>
                </tr>
            @endforeach
            </table>
        @endif
    </div>

    <div class="text-center"><a href="{{ route('main_welcome', app()->getLocale()) }}">Back to main Page</a></div>


@endsection
