@extends('layouts.main_app')

@section('user_form')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif


    <div class="text-center"><h3>USER FORM</h3></div>

    {!! Form::open(['url'=>route('user_create'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

    {{--NAME--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('name', 'User name', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('name', old('name'), ['class' => 'form-control',
                                        'placeholder'=>'Input user name']) !!}
            </div>
        </div>
    </div>

    {{--EMAIL--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('email', 'Email', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::email('email', old('email'), ['class' => 'form-control',
                                        'placeholder'=>'Input user email']) !!}
            </div>
        </div>
    </div>

    {{--password--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('password', 'Password', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::password('password', old('password'), ['class' => 'form-control',
                                        'placeholder'=>'Input user password']) !!}
            </div>
        </div>
    </div>

    {{--SAVE BUTTON--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-1 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="text-center"><a href="{{ route('user') }}">Admin Panel</a></div>


@endsection
