@extends('layouts.main_app')

@section('articles_list')

    <div class="text-center"><h4>Articles List</h4></div>
    <div class="container-fluid">
        @if ($articles)
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>author</th>
                    <th>title</th>
                    <th>text</th>
                    <th>Data Created</th>
                    <th>Delete</th>
                </tr>
                </thead>
            @foreach ($articles as $article)
                <tr>
                    <td>{{ $article->id}}</td>
                    <td>{{ $article->author }}</td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->content }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td>
                        {{--Delete--}}
                        {!!  Form::open(['url' => route('article_edit', ['article'=>$article->id]), 'class'=>'form-horizontal', 'method'=>'POST']) !!}
                        {{ method_field('DELETE') }}
                        {!! Form::button('Delete', ['class'=>'btn btn-danger', 'type'=>'submit'])!!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </table>
        @endif
    </div>

    <div class="text-center"><a href="{{ route('main_welcome', app()->getLocale()) }}">Back to main Page</a></div>


@endsection
