@extends('layouts.main_app')

@section('article_form')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif







    {{--<pre>--}}
        {{--{{ print_r(Session::all()) }}--}}
    {{--</pre>--}}






    <div class="text-center"><h3>ARTICLE FORM</h3></div>

    {!! Form::open(['url'=>route('article_create'),'class'=>'form-horizontal','method'=>'POST','enctype'=>'multipart/form-data']) !!}

    {{--TITLE--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-1">
                {!! Form::label('title', 'Article Title', ['class' => 'control-label']) !!}
            </div>
            <div class="col-sm-8">
                {!! Form::text('title', old('title'), ['class' => 'form-control',
                                        'placeholder'=>'Input article title']) !!}
                {{--<input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}">--}}
            </div>
        </div>
    </div>

    {{--CONTENT--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-1"></div>
            {!! Form::label('content', 'Content', ['class' => 'col-sm-1 control-label']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('content', old('content'), ['class' => 'form-control',
                                        'placeholder'=>'Input article text']) !!}
            </div>
        </div>
    </div>

    {{--SAVE BUTTON--}}
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-offset-1 col-sm-8">
                {!! Form::button('Save', ['class' => 'btn btn-primary', 'type'=>'submit']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="text-center"><a href="{{ route('main_welcome', app()->getLocale()) }}">Back to main Page</a></div>









@endsection
