@extends('layouts.main_app')


    {{--<pre>--}}
        {{--{{ print_r(Session::all()) }}--}}
    {{--</pre>--}}


{{--<ul class="nav-bar ml-auto">--}}
    {{--<li class="nav-item">--}}
        {{--<a href="{{ route(Route::currentRouteName(), 'en') }}" class="nav-link">EN</a>--}}
    {{--</li>--}}

    {{--<li class="nav-item">--}}
        {{--<a href="{{ route(Route::currentRoutename(), 'ru') }}" class="nav-link">RU</a>--}}
    {{--</li>--}}
{{--</ul>--}}



@section('auth')
    <div class="flex-center position-ref full-height">

        @if (Route::has('login'))
            <div class="text-center">

                @auth
                    <a  href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
{{--                        {{ __('messages.Logout') }}--}}
                    </a>

                    <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                    <a href="{{ route('article_create', app()->getLocale()) }}">{{ __("Create article") }}</a>

                    <a href="{{ route('articles_list', app()->getLocale()) }}">Articles list</a>

                    <div class="text-center"><a href="{{ route('user') }}">{{__('Admin Panel')}}</a></div>

                    <div class="text-center"><a href="{{ route('send_email') }}">Send Email</a></div>

                @else
                    <a href="{{ route('login', app()->getLocale()) }}">Login</a>
{{--                    <a href="{{ route('login') }}">{{ __('messages.Login') }}</a>--}}
{{--                    <a href="{{ route('register') }}">{{ __('messages.Register') }}</a>--}}
                    <a href="{{ route('register', app()->getLocale()) }}"> @lang('messages.Register') </a>
                @endauth

            </div>
        @endif

    </div>
@endsection



@section('articles')

    @if($articles)
        <div class="text-center"><h4>Articles List</h4></div>
        @foreach( $articles as $article)
            <div class="container">
                <div class="row">
                    <div class="col"></div>
                    <div class="col text-center">
                        <div class="card" style="">
                            <div class="card-body">
                                <h5 class="card-title">{{ $article->title }}</h5>
                                <p class="text-left" style="font-size: 10px">{{ $article->author }}</p>
                                <p class="card-text">{{ $article->content }}</p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center"><h4>NO Articles List</h4></div>
    @endif

@endsection

