<?php

return [
    "welcome" => "Welcome",
    "hello"   => "hello",
    "Logout"  => "Logout",
    "Login"  => "Login",
    "Register" => "Register",
    "main_page" => "Main Page",
];
