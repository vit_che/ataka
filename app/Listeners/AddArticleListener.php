<?php

namespace App\Listeners;

use App\Events\AddArticleEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class AddArticleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddArticleEvent  $event
     * @return void
     */
    public function handle(AddArticleEvent $event)
    {
        Log::info('Article Title', [ $event->user_name, $event->article_title]);
    }
}
