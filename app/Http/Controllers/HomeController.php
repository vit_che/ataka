<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $articles = Article::all();
//        $data = [ 'articles' => $articles];

        dump("test");
        $ext = new DateCheck();
        dump($ext->isValid('23/02/1999'));


        return view('main_welcome', $data);
    }

    public function email(Request $request)
    {
        if ($request->isMethod('post')) {

            $input = $request->except('_token');

            Mail::send('emails.mail_test', ['data' => $input], function($message) use($input){

                $message->from($input['email'], $input['name']);
                $message->to($input['email'])->subject('Question');
            });

            return redirect('/')->with('status', 'The mail was sent');
        }

        return view('send_email');

    }

}
