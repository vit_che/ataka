<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Validator;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $users = User::all();
        $data = ["users" => $users];
        return view('admin.users.users', $data);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $user = new User();

            $input['password'] = bcrypt($input['password']);

            $user->fill($input);

            $messages = [
                'required' => 'The field :attribute req',
            ];

            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'email' => 'required',
            ], $messages);

            if ($validator->fails()) {
                return redirect()->route('article_create')->withErrors($validator)->withInput();
            }

            if($user->save()){
                return redirect('/admin/user')->with('status', 'User was create');
//                return redirect()->route('main_welcome')->with('status', 'Article was create');
            }
        }

        return view('admin.users.create');
    }
}
