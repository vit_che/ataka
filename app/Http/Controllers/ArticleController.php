<?php

namespace App\Http\Controllers;

use App\Events\AddArticleEvent;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Article;
use Validator;




class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();

        $data = [ "articles" => $articles ];

        return view('admin.articles.articles',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $author = $user['name'];

        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $article = new Article();
            $article->author = $author;
            $article->fill($input);
            $article->user_id = $user["id"];

            $messages = [
                'required' => 'The field :attribute req',
            ];

            $validator = Validator::make($input, [
                'title' => 'required|max:255',
                'content' => 'required',
            ], $messages);

            $request->flash();

            if ($validator->fails()) {

                return redirect()->route('article_create')->withErrors($validator)->withInput();
            }

            if($article->save()){

                event(new AddArticleEvent($article, $user));

                return redirect('/')->with('status', 'Article was create');
//                return redirect('/article/create')->with('status', 'Article was create');
//                return redirect()->route('main_welcome')->with('status', 'Article was create');
            }
        }

        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $articles = Article::all();
        $article = Article::find($id);

        if ($request->isMethod('DELETE')) {
            $article->delete();

            return redirect('admin/articles')->with('status', 'The Article was deleted');
        }

        $data = ["articles" => $articles];

        return view('admin.articles.articles', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
