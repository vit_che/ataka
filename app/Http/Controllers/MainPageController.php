<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Services\DateCheck;
use DateService;
use App;


class MainPageController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function index(Request $request)
    {
        $articles = Article::all();
        $data = [ 'articles' => $articles];

        $locale = App::getLocale();

//        dump($locale);
        $ext = new DateCheck();
//        dump($ext->isValid('23/02/1999'));
//        dump(DateService::isValid('23/02/1999'));


        return view('main_welcome', $data);
    }

    public function testone()
    {


        return view('test');
    }





}
